I was able to implement the general board and moves in the 3-4 hour window. Following is the command line UI.

             A          B          C          D          E          F          G          H
         8    B. Rook  B. Knight  B. Bishop   B. Queen    B. King  B. Bishop  B. Knight    B. Rook
         7    B. Pawn    B. Pawn    B. Pawn    B. Pawn    B. Pawn    B. Pawn    B. Pawn    B. Pawn
         6          -          -          -          -          -          -          -          -
         5          -          -          -          -          -          -          -          -
         4          -          -          -          -          -          -          -          -
         3          -          -          -          -          -          -          -          -
         2    W. Pawn    W. Pawn    W. Pawn    W. Pawn    W. Pawn    W. Pawn    W. Pawn    W. Pawn
         1    W. Rook  W. Knight  W. Bishop   W. Queen    W. King  W. Bishop  W. Knight    W. Rook
                    A          B          C          D          E          F          G          H
WHITE Player's turn
Enter the piece position you want to move :

Validations and rules are implemented for pawns and rooks. Other pieces should be played with the general rules in mind.