package com.alice.chess;


public class Player {
    private Integer no;
    private Color color;

    public Player(int no, Color color) {
        this.no = no;
        this.color = color;
    }
}
