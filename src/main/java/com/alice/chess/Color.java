package com.alice.chess;

public enum Color {
    WHITE, BLACK, NO_COLOR;
}
