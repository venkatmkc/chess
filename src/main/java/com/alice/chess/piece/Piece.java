package com.alice.chess.piece;


import com.alice.chess.Color;

public abstract class Piece {
    Color color;
    int x, y;

    public Piece(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    @Override
    public String toString() {
        switch (color) {
            case WHITE:
                return "W.";
            case BLACK:
                return "B.";
            default:
                return "-";
        }
    }

    public boolean isOfColor(Color color) {
        return this.color.equals(color);
    }

    public void movePiece(Piece to, Piece[][] pieces) {
        pieces[to.x][to.y] = pieces[x][y];
        pieces[x][y] = new NoPiece(x, y, Color.NO_COLOR);
    }

}
