package com.alice.chess.piece;

import com.alice.chess.Color;

public class Rook extends Piece {
    public Rook(int x, int y, Color color) {
        super(x, y, color);
    }


    @Override
    public String toString() {
        return super.toString() + " Rook";
    }

    @Override
    public void movePiece(Piece to, Piece[][] pieces) {
        if(x == to.x && y != to.y && (color != to.color)) {
            if(y < to.y) {
                for(int i = to.y-1; i > y; i--) {
                    if(!(pieces[x][i] instanceof NoPiece)) {
                        throw new RuntimeException("Invalid move");
                    }
                }
            } else {
                for(int i = to.y+1; i < y; i++) {
                    if(!(pieces[x][i] instanceof NoPiece)) {
                        throw new RuntimeException("Invalid move");
                    }
                }
            }
            pieces[to.x][to.y] = pieces[x][y];
            pieces[x][y] = new NoPiece(x, y, Color.NO_COLOR);
        } else if(y == to.y && x != to.x && (color != to.color)) {
            if(x < to.x) {
                for(int i = to.x-1; i > x; i--) {
                    if(!(pieces[y][i] instanceof NoPiece)) {
                        throw new RuntimeException("Invalid move");
                    }
                }
            } else {
                for(int i = to.x+1; i < x; i++) {
                    if(!(pieces[y][i] instanceof NoPiece)) {
                        throw new RuntimeException("Invalid move");
                    }
                }
            }
            pieces[to.x][to.y] = pieces[x][y];
            pieces[x][y] = new NoPiece(x, y, Color.NO_COLOR);
        } else {
            throw new RuntimeException("Invalid move");
        }
    }
}
