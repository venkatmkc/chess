package com.alice.chess.piece;

import com.alice.chess.Color;

public class Pawn extends Piece {
    public Pawn(int x, int y, Color color) {
        super(x, y, color);
    }

    @Override
    public String toString() {
        return super.toString() + " Pawn";
    }

    @Override
    public void movePiece(Piece to, Piece[][] pieces) {
        if(isOfColor(Color.WHITE)) {
            if(to.x == x && to.y == y+1 && pieces[to.x][to.y] instanceof NoPiece) {
                pieces[to.x][to.y] = pieces[x][y];
                pieces[x][y] = new NoPiece(x, y, Color.NO_COLOR);
            } else if(to.x == x-1 && to.y == y+1 && pieces[to.x][to.y].isOfColor(Color.BLACK)) {
                pieces[to.x][to.y] = pieces[x][y];
                pieces[x][y] = new NoPiece(x, y, Color.NO_COLOR);
            } else if(to.x == x+1 && to.y == y+1 && pieces[to.x][to.y].isOfColor(Color.BLACK)) {
                pieces[to.x][to.y] = pieces[x][y];
                pieces[x][y] = new NoPiece(x, y, Color.NO_COLOR);
            }
        } else {
            if(to.x == x && to.y == y-1 && pieces[to.x][to.y] instanceof NoPiece) {
                pieces[to.x][to.y] = pieces[x][y];
                pieces[x][y] = new NoPiece(x, y, Color.NO_COLOR);
            } else if(to.x == x-1 && to.y == y-1 && pieces[to.x][to.y].isOfColor(Color.WHITE)) {
                pieces[to.x][to.y] = pieces[x][y];
                pieces[x][y] = new NoPiece(x, y, Color.NO_COLOR);
            } else if(to.x == x+1 && to.y == y-1 && pieces[to.x][to.y].isOfColor(Color.WHITE)) {
                pieces[to.x][to.y] = pieces[x][y];
                pieces[x][y] = new NoPiece(x, y, Color.NO_COLOR);
            }
        }
    }
}
