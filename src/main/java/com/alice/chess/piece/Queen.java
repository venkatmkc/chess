package com.alice.chess.piece;

import com.alice.chess.Color;

public class Queen extends Piece {
    public Queen(int x, int y, Color color) {
        super(x, y, color);
    }

    @Override
    public String toString() {
        return super.toString() + " Queen";
    }

}
