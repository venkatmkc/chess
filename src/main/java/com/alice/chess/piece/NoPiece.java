package com.alice.chess.piece;

import com.alice.chess.Color;

public class NoPiece extends Piece {
    public NoPiece(int x, int y, Color color) {
        super(x, y, color);
    }

}
