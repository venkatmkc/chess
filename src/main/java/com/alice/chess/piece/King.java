package com.alice.chess.piece;

import com.alice.chess.Color;

public class King extends Piece {
    public King(int x, int y, Color color) {
        super(x, y, color);
    }

    @Override
    public String toString() {
        return super.toString() + " King";
    }

}
