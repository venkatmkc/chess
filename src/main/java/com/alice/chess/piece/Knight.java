package com.alice.chess.piece;

import com.alice.chess.Color;

public class Knight extends Piece {
    public Knight(int x, int y, Color color) {
        super(x, y, color);
    }

    @Override
    public String toString() {
        return super.toString() + " Knight";
    }

}
