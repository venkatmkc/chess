package com.alice.chess.piece;


import com.alice.chess.Color;

public class Bishop extends Piece {
    public Bishop(int x, int y, Color color) {
        super(x, y, color);
    }

    @Override
    public String toString() {
        return super.toString() + " Bishop";
    }

}
