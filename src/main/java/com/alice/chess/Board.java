package com.alice.chess;

import com.alice.chess.piece.*;


public class Board {

    private Piece[][] pieces;

    public Board() {
        pieces = new Piece[8][8];

        //White pieces
        pieces[0][0] = new Rook(0, 0, Color.WHITE);
        pieces[1][0] = new Knight(1, 0, Color.WHITE);
        pieces[2][0] = new Bishop(2, 0,Color.WHITE);
        pieces[3][0] = new Queen(3, 0, Color.WHITE);
        pieces[4][0] = new King(4, 0, Color.WHITE);
        pieces[5][0] = new Bishop(5, 0,Color.WHITE);
        pieces[6][0] = new Knight(6, 0, Color.WHITE);
        pieces[7][0] = new Rook(7, 0, Color.WHITE);

        //Black pieces
        pieces[0][7] = new Rook(0, 7, Color.BLACK);
        pieces[1][7] = new Knight(1, 7, Color.BLACK);
        pieces[2][7] = new Bishop(2, 7, Color.BLACK);
        pieces[3][7] = new Queen(3, 7, Color.BLACK);
        pieces[4][7] = new King(4, 7, Color.BLACK);
        pieces[5][7] = new Bishop(5, 7, Color.BLACK);
        pieces[6][7] = new Knight(6, 7, Color.BLACK);
        pieces[7][7] = new Rook(7, 7, Color.BLACK);

        //Pawns and empty places
        for(int i = 0; i < 8; i++) {
            pieces[i][1] = new Pawn(i, 1, Color.WHITE);
            pieces[i][2] = new NoPiece(i, 2, Color.NO_COLOR);
            pieces[i][3] = new NoPiece(i, 3, Color.NO_COLOR);
            pieces[i][4] = new NoPiece(i, 4, Color.NO_COLOR);
            pieces[i][5] = new NoPiece(i, 5, Color.NO_COLOR);
            pieces[i][6] = new Pawn(i, 6, Color.BLACK);
        }

    }

    //Print the board using current pieces
    public void print() {
        System.out.printf("%10s %10s %10s %10s %10s %10s %10s %10s %10s\n", " ", "A", "B", "C", "D", "E", "F", "G", "H");
        for(int i = 7; i >= 0 ; i--) {
            System.out.printf("%10s", i+1);
            for(int j = 0; j < 8; j++) {
                System.out.printf(" %10s", pieces[j][i]);
            }
            System.out.println();
        }
        System.out.printf("%10s %10s %10s %10s %10s %10s %10s %10s %10s\n", " ", "A", "B", "C", "D", "E", "F", "G", "H");
    }


    public Piece getPiece(String Piece) {
        return pieces[Piece.charAt(0)-'A'][Piece.charAt(1)-'1'];
    }

    public boolean hasPieceWithColor(int fromX, int  fromY, Color color) {
        Piece piece = pieces[fromX][fromY];
        if(piece instanceof NoPiece) {
            return false;
        }
        return !piece.isOfColor(color);
    }

    public Piece getPosition(String position) {
        return pieces[position.charAt(0)-'A'][position.charAt(1)-'1'];
    }

    public void movePiece(Piece from, Piece to) {
        from.movePiece(to, pieces);
    }
}
