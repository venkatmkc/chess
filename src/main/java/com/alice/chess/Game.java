package com.alice.chess;

import com.alice.chess.piece.NoPiece;
import com.alice.chess.piece.Piece;

import java.util.Scanner;

public class Game {

    private final Scanner input;
    private Board board;

    public Game() {
        this.input = new Scanner(System.in);
        this.board = new Board();
    }

    //Game loop
    public void start() {
        boolean isWhitePlayerTurn = true;
        while (true) {
            try {
                board.print();
                Color color = isWhitePlayerTurn ? Color.WHITE : Color.BLACK;
                System.out.println(color + " Player's turn");
                System.out.println("Enter the piece position you want to move : ");
                String position = input.next();
                Piece from = parsePiece(position);
                checkValidFrom(from, isWhitePlayerTurn);
                System.out.print("Enter the piece destination position you want to move to : ");
                position = input.next();
                Piece to = parsePiece(position);
                board.movePiece(from, to);
            } catch(RuntimeException e) {
                System.out.println(e.getMessage());
                continue;
            }
            isWhitePlayerTurn = !isWhitePlayerTurn;
        }
    }

    //Checks if there is a valid piece to move
    private void checkValidFrom(Piece piece, boolean isWhitePlayerTurn) {
        if(!hasPieceWithColor(piece, isWhitePlayerTurn)) {
            throw new RuntimeException("No valid piece in that position");
        }
    }

    private boolean hasPieceWithColor(Piece piece, boolean isWhitePlayerTurn) {
        Color color = isWhitePlayerTurn ? Color.WHITE : Color.BLACK;
        if(piece instanceof NoPiece) {
            return false;
        }
        return piece.isOfColor(color);
    }

    private Piece parsePiece(String position) {
        if(position.length() != 2) {
            throw new RuntimeException("Invalid position length");
        }
        if(position.charAt(0) < 'A' || position.charAt(0) > 'H' || position.charAt(1) < '1' || position.charAt(1) > '8') {
            throw new RuntimeException("Invalid position");
        }
        Piece piece = board.getPosition(position);
        return piece;
    }

}
